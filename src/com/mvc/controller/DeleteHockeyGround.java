package com.mvc.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mvc.bean.DeleteGroundBean;
import com.mvc.bean.DeleteHockeyGroundBean;
import com.mvc.dao.DeleteGroundDao;
import com.mvc.dao.DeleteHockeyGroundDao;

/**
 * Servlet implementation class Delete_Author_Servlet
 */
@WebServlet("/DeleteHockeyGround")
public class DeleteHockeyGround extends HttpServlet {
	private static final long serialVersionUID = 1L;
      
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		String groundname=request.getParameter("ground");
		System.out.println("in servlet"+groundname);
		DeleteHockeyGroundBean DeleteHockeyGroundBean = new DeleteHockeyGroundBean();

		System.out.println(" before"+groundname);
		DeleteHockeyGroundBean.setgroundname(groundname);
		System.out.println("after");
		DeleteHockeyGroundDao DeleteHockeyGroundDao = new DeleteHockeyGroundDao();
		System.out.println("123");
		String DeleteHockeyGround = DeleteHockeyGroundDao.DeleteHockeyGround(DeleteHockeyGroundBean);
		
		if (DeleteHockeyGround.equals("SUCCESS")) // On success, you can display a message to user on Home page
		{
			System.out.println("in if");
			//request.setAttribute("Admin_Menu", true);

			request.getRequestDispatcher("/Hockey.jsp").forward(request, response);
		} else // On Failure, display a meaningful message to the User.
		{
			System.out.println("in else");
			
			request.setAttribute("errMessage", DeleteHockeyGround);
			request.getRequestDispatcher("/DeleteHockeyGround.jsp").forward(request, response);
		}
	}
	}


