package com.mvc.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mvc.bean.DeleteRugbyGroundBean;
import com.mvc.dao.DeleteRugbyGroundDao;

/**
 * Servlet implementation class Delete_Author_Servlet
 */
@WebServlet("/DeleteRugbyGround")
public class DeleteRugbyGround extends HttpServlet {
	private static final long serialVersionUID = 1L;
      
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		String groundname=request.getParameter("ground");
		System.out.println("in servlet"+groundname);
		DeleteRugbyGroundBean DeleteRugbyGroundBean = new DeleteRugbyGroundBean();

		System.out.println(" before"+groundname);
		DeleteRugbyGroundBean.setgroundname(groundname);
		System.out.println("after");
		DeleteRugbyGroundDao DeleteRugbyGroundDao = new DeleteRugbyGroundDao();
		System.out.println("123");
		String DeleteRugbyGround = DeleteRugbyGroundDao.DeleteRugbyGround(DeleteRugbyGroundBean);
		
		if (DeleteRugbyGround.equals("SUCCESS")) // On success, you can display a message to user on Home page
		{
			System.out.println("in if");
			//request.setAttribute("Admin_Menu", true);

			request.getRequestDispatcher("/Rugby.jsp").forward(request, response);
		} else // On Failure, display a meaningful message to the User.
		{
			System.out.println("in else");
			
			request.setAttribute("errMessage", DeleteRugbyGround);
			request.getRequestDispatcher("/DeleteRugbyGround.jsp").forward(request, response);
		}
	}
	}


