package com.mvc.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mvc.bean.PlayersRegistrationBean;
import com.mvc.dao.PlayersRegistrationDao;

@WebServlet("/PlayersRegistration")
public class PlayersRegistration extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public PlayersRegistration() {
		super();

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		
		String username = request.getParameter("username");
		String gender = request.getParameter("gender");
		String age = request.getParameter("age");
		String phonenumber = request.getParameter("phonenumber");
		String password = request.getParameter("password");
		String address = request.getParameter("address");

		PlayersRegistrationBean playersregistrationBean = new PlayersRegistrationBean();

		
		playersregistrationBean.setUserName(username);
		playersregistrationBean.setGender(gender);
		playersregistrationBean.setAge(age);
		playersregistrationBean.setPhoneNumber(phonenumber);
		playersregistrationBean.setPassword(password);
		playersregistrationBean.setAddress(address);

		PlayersRegistrationDao playersregistrationDao = new PlayersRegistrationDao();

		String userRegistered = playersregistrationDao.playersregistrationUser(playersregistrationBean);
		if (userRegistered.equals("SUCCESS")) // On success, you can display a message to user on Home page
		{

			request.setAttribute("adminpage", true);

			request.getRequestDispatcher("/PlayersLogin.jsp").forward(request, response);
		} else // On Failure, display a meaningful message to the User.
		{
			request.setAttribute("playersrgstr", false);
			request.setAttribute("errMessage", userRegistered);
			request.getRequestDispatcher("/PlayersRegistration.jsp").forward(request, response);
		}

	}

}
