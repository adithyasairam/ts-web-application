package com.mvc.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mvc.bean.CancelBookingsBean;
import com.mvc.dao.CancelBookingsDao;

/**
 * Servlet implementation class Delete_Author_Servlet
 */
@WebServlet("/CancelBookings")
public class CancelBookings extends HttpServlet {
	private static final long serialVersionUID = 1L;
      
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		String groundname=request.getParameter("ground");
		System.out.println("in servlet"+groundname);
		CancelBookingsBean CancelBookingsBean = new CancelBookingsBean();

		System.out.println(" before"+groundname);
		CancelBookingsBean.setgroundname(groundname);
		System.out.println("after");
		CancelBookingsDao CancelBookingsDao = new CancelBookingsDao();
		System.out.println("123");
		String CancelBookings = CancelBookingsDao.CancelBookings(CancelBookingsBean);
		
		if (CancelBookings.equals("SUCCESS")) // On success, you can display a message to user on Home page
		{
			System.out.println("in if");
			//request.setAttribute("Admin_Menu", true);

			request.getRequestDispatcher("/PlayersPage.jsp").forward(request, response);
		} else // On Failure, display a meaningful message to the User.
		{
			System.out.println("in else");
			
			request.setAttribute("errMessage", CancelBookings);
			request.getRequestDispatcher("/CancelBooking.jsp").forward(request, response);
		}
	}
	}


