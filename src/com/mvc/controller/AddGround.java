package com.mvc.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mvc.bean.AddGroundBean;
import com.mvc.dao.AddGroundDao;

@WebServlet("/AddGround")
public class AddGround extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public AddGround() {
		super();

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		
		String groundname = request.getParameter("groundname");
		String area = request.getParameter("area");
		String city = request.getParameter("city");
		String cost = request.getParameter("cost");
		String address = request.getParameter("address");

		AddGroundBean AddGroundBean = new AddGroundBean();

		
		AddGroundBean.setGroundName(groundname);
		AddGroundBean.setArea(area);
		AddGroundBean.setCity(city);
		AddGroundBean.setCost(cost);
		AddGroundBean.setAddress(address);

		AddGroundDao AddGroundDao = new AddGroundDao();

		String AddGround = AddGroundDao.AddGround(AddGroundBean);
		if (AddGround.equals("SUCCESS")) // On success, you can display a message to user on Home page
		{

			request.setAttribute("Cricket", true);

			request.getRequestDispatcher("/Cricket.jsp").forward(request, response);
		} else // On Failure, display a meaningful message to the User.
		{
			request.setAttribute("AddGround", false);
			request.setAttribute("errMessage", AddGround);
			request.getRequestDispatcher("/AddGround.jsp").forward(request, response);
		}

	}

}
