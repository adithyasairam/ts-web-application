package com.mvc.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mvc.bean.AddFootballGroundBean;
import com.mvc.dao.AddFootballGroundDao;

@WebServlet("/AddFootballGround")
public class AddFootballGround extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public AddFootballGround() {
		super();

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		
		String groundname = request.getParameter("groundname");
		String area = request.getParameter("area");
		String city = request.getParameter("city");
		String cost = request.getParameter("cost");
		String address = request.getParameter("address");

		AddFootballGroundBean AddFootballGroundBean = new AddFootballGroundBean();

		
		AddFootballGroundBean.setGroundName(groundname);
		AddFootballGroundBean.setArea(area);
		AddFootballGroundBean.setCity(city);
		AddFootballGroundBean.setCost(cost);
		AddFootballGroundBean.setAddress(address);

		AddFootballGroundDao AddFootballGroundDao = new AddFootballGroundDao();

		String AddFootballGround = AddFootballGroundDao.AddFootballGround(AddFootballGroundBean);
		if (AddFootballGround.equals("SUCCESS")) // On success, you can display a message to user on Home page
		{

			request.setAttribute("Football", true);

			request.getRequestDispatcher("/Football.jsp").forward(request, response);
		} else // On Failure, display a meaningful message to the User.
		{
			request.setAttribute("AddFootballGround", false);
			request.setAttribute("errMessage", AddFootballGround);
			request.getRequestDispatcher("/AddFootballGround.jsp").forward(request, response);
		}

	}

}
