package com.mvc.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mvc.bean.AddRugbyGroundBean;
import com.mvc.dao.AddRugbyGroundDao;

@WebServlet("/AddRugbyGround")
public class AddRugbyGround extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public AddRugbyGround() {
		super();

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		
		String groundname = request.getParameter("groundname");
		String area = request.getParameter("area");
		String city = request.getParameter("city");
		String cost = request.getParameter("cost");
		String address = request.getParameter("address");

		AddRugbyGroundBean AddRugbyGroundBean = new AddRugbyGroundBean();

		
		AddRugbyGroundBean.setGroundName(groundname);
		AddRugbyGroundBean.setArea(area);
		AddRugbyGroundBean.setCity(city);
		AddRugbyGroundBean.setCost(cost);
		AddRugbyGroundBean.setAddress(address);

		AddRugbyGroundDao AddRugbyGroundDao = new AddRugbyGroundDao();

		String AddRugbyGround = AddRugbyGroundDao.AddRugbyGround(AddRugbyGroundBean);
		if (AddRugbyGround.equals("SUCCESS")) // On success, you can display a message to user on Home page
		{

			request.setAttribute("Rugby", true);

			request.getRequestDispatcher("/Rugby.jsp").forward(request, response);
		} else // On Failure, display a meaningful message to the User.
		{
			request.setAttribute("AddRugbyGround", false);
			request.setAttribute("errMessage", AddRugbyGround);
			request.getRequestDispatcher("/AddRugbyGround.jsp").forward(request, response);
		}

	}

}
