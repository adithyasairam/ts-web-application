package com.mvc.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mvc.bean.DeleteGroundBean;
import com.mvc.dao.DeleteGroundDao;

/**
 * Servlet implementation class Delete_Author_Servlet
 */
@WebServlet("/DeleteGround")
public class DeleteGround extends HttpServlet {
	private static final long serialVersionUID = 1L;
      
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		String groundname=request.getParameter("ground");
		System.out.println("in servlet"+groundname);
		DeleteGroundBean DeleteGroundBean = new DeleteGroundBean();

		System.out.println(" before"+groundname);
		DeleteGroundBean.setgroundname(groundname);
		System.out.println("after");
		DeleteGroundDao DeleteGroundDao = new DeleteGroundDao();
		System.out.println("123");
		String DeleteGround = DeleteGroundDao.DeleteGround(DeleteGroundBean);
		
		if (DeleteGround.equals("SUCCESS")) // On success, you can display a message to user on Home page
		{
			System.out.println("in if");
			//request.setAttribute("Admin_Menu", true);

			request.getRequestDispatcher("/Cricket.jsp").forward(request, response);
		} else // On Failure, display a meaningful message to the User.
		{
			System.out.println("in else");
			
			request.setAttribute("errMessage", DeleteGround);
			request.getRequestDispatcher("/DeleteGround.jsp").forward(request, response);
		}
	}
	}


