package com.mvc.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mvc.bean.AddHockeyGroundBean;
import com.mvc.dao.AddHockeyGroundDao;

@WebServlet("/AddHockeyGround")
public class AddHockeyGround extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public AddHockeyGround() {
		super();

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		
		String groundname = request.getParameter("groundname");
		String area = request.getParameter("area");
		String city = request.getParameter("city");
		String cost = request.getParameter("cost");
		String address = request.getParameter("address");

		AddHockeyGroundBean AddHockeyGroundBean = new AddHockeyGroundBean();

		
		AddHockeyGroundBean.setGroundName(groundname);
		AddHockeyGroundBean.setArea(area);
		AddHockeyGroundBean.setCity(city);
		AddHockeyGroundBean.setCost(cost);
		AddHockeyGroundBean.setAddress(address);

		AddHockeyGroundDao AddHockeyGroundDao = new AddHockeyGroundDao();

		String AddHockeyGround = AddHockeyGroundDao.AddHockeyGround(AddHockeyGroundBean);
		if (AddHockeyGround.equals("SUCCESS")) // On success, you can display a message to user on Home page
		{

			request.setAttribute("Hockey", true);

			request.getRequestDispatcher("/Hockey.jsp").forward(request, response);
		} else // On Failure, display a meaningful message to the User.
		{
			request.setAttribute("AddHockeyGround", false);
			request.setAttribute("errMessage", AddHockeyGround);
			request.getRequestDispatcher("/AddHockeyGround.jsp").forward(request, response);
		}

	}

}
