package com.mvc.bean;

public class AdminLoginBean {

	// It contains only setters and getters
		private String username;
		private String password;

		public String getUserName() {
			return username;
		}

		public void setUserName(String username) {
			this.username = username;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}
	}

	


