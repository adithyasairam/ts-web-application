package com.mvc.bean;

public class AddRugbyGroundBean {
	private String groundname;
	private String area;
	private String city;
	private String cost;
	private String address;
	

	public String getGroundName() {
		return groundname;
	}

	public void setGroundName(String groundname) {
		this.groundname = groundname;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
}
