package com.mvc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.mvc.bean.AddRugbyGroundBean;
import com.mvc.util.DBConnection;

public class AddRugbyGroundDao {
	public String AddRugbyGround(AddRugbyGroundBean AddRugbyGroundBean)

	{

		
		String groundname = AddRugbyGroundBean.getGroundName();
		String area = AddRugbyGroundBean.getArea();
		String city = AddRugbyGroundBean.getCity();
		String cost = AddRugbyGroundBean.getCost();
		String address = AddRugbyGroundBean.getAddress();

		Connection con = null;

		PreparedStatement preparedstatement = null;

		try

		{
			con = DBConnection.createConnection();

			String query = "insert into rugbygrounds (groundname,area,city,cost,address) values (?,?,?,?,?)";

			preparedstatement = con.prepareStatement(query);

			
			preparedstatement.setString(1, groundname);
			preparedstatement.setString(2, area);
			preparedstatement.setString(3, city);
			preparedstatement.setString(4, cost);
			preparedstatement.setString(5, address);

			int i = preparedstatement.executeUpdate();

			if (i != 0)

				return "SUCCESS";

		} catch (SQLException e) {
			e.printStackTrace();

		}
		return "UNSUCCESS";

	}
}
