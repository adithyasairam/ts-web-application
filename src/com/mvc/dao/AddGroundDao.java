package com.mvc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.mvc.bean.AddGroundBean;
import com.mvc.bean.PlayersRegistrationBean;
import com.mvc.util.DBConnection;

public class AddGroundDao {
	public String AddGround(AddGroundBean AddGroundBean)

	{

		
		String groundname = AddGroundBean.getGroundName();
		String area = AddGroundBean.getArea();
		String city = AddGroundBean.getCity();
		String cost = AddGroundBean.getCost();
		String address = AddGroundBean.getAddress();

		Connection con = null;

		PreparedStatement preparedstatement = null;

		try

		{
			con = DBConnection.createConnection();

			String query = "insert into cricketgrounds (groundname,area,city,cost,address) values (?,?,?,?,?)";

			preparedstatement = con.prepareStatement(query);

			
			preparedstatement.setString(1, groundname);
			preparedstatement.setString(2, area);
			preparedstatement.setString(3, city);
			preparedstatement.setString(4, cost);
			preparedstatement.setString(5, address);

			int i = preparedstatement.executeUpdate();

			if (i != 0)

				return "SUCCESS";

		} catch (SQLException e) {
			e.printStackTrace();

		}
		return "UNSUCCESS";

	}
	
	
}

