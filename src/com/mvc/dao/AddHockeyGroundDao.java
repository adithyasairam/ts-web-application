package com.mvc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.mvc.bean.AddHockeyGroundBean;
import com.mvc.util.DBConnection;

public class AddHockeyGroundDao {
	public  String AddHockeyGround(AddHockeyGroundBean AddHockeyGroundBean)

	{

		
		String groundname = AddHockeyGroundBean.getGroundName();
		String area = AddHockeyGroundBean.getArea();
		String city = AddHockeyGroundBean.getCity();
		String cost = AddHockeyGroundBean.getCost();
		String address = AddHockeyGroundBean.getAddress();

		Connection con = null;

		PreparedStatement preparedstatement = null;

		try

		{
			con = DBConnection.createConnection();

			String query = "insert into hockeygrounds (groundname,area,city,cost,address) values (?,?,?,?,?)";

			preparedstatement = con.prepareStatement(query);

			
			preparedstatement.setString(1, groundname);
			preparedstatement.setString(2, area);
			preparedstatement.setString(3, city);
			preparedstatement.setString(4, cost);
			preparedstatement.setString(5, address);

			int i = preparedstatement.executeUpdate();

			if (i != 0)

				return "SUCCESS";

		} catch (SQLException e) {
			e.printStackTrace();

		}
		return "UNSUCCESS";

	}
}
