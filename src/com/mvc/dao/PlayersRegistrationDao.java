package com.mvc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.mvc.bean.PlayersRegistrationBean;
import com.mvc.util.DBConnection;

public class PlayersRegistrationDao {

	public String playersregistrationUser(PlayersRegistrationBean playersregistrationBean)

	{

		
		String username = playersregistrationBean.getUserName();
		String gender = playersregistrationBean.getGender();
		String age = playersregistrationBean.getAge();
		String phonenumber = playersregistrationBean.getPhoneNumber();
		String password = playersregistrationBean.getPassword();
		String address = playersregistrationBean.getAddress();

		Connection con = null;

		PreparedStatement preparedstatement = null;

		try

		{
			con = DBConnection.createConnection();

			String query = "insert into players (playerid,username,gender,age,phonenumber,password,address) values (adithya.nextval,?,?,?,?,?,?)";

			preparedstatement = con.prepareStatement(query);

			
			preparedstatement.setString(1, username);
			preparedstatement.setString(2, gender);
			preparedstatement.setString(3, age);
			preparedstatement.setString(4, phonenumber);
			preparedstatement.setString(5, password);
			preparedstatement.setString(6, address);

			int i = preparedstatement.executeUpdate();

			if (i != 0)

				return "SUCCESS";

		} catch (SQLException e) {
			e.printStackTrace();

		}
		return "UNSUCCESS";

	}

}