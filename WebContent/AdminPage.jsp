<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Sports Club Management System</title>
<link rel="stylesheet" type="text/css" href="Stylesheet_AdminIndex.css">

</head>

<%--Html--%>

<body>
	<div class="navbar">
		<div class="dropdown">
    <button class="dropbtn">Grounds 
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="Cricket.jsp">Cricket</a>
      <a href="Football.jsp">Football</a>
      <a href="Hockey.jsp">Hockey</a>
      <a href="Rugby.jsp">Rugby</a>
      
    </div>
  </div> 
		
		
		
		
		
		 <a href="ViewBookedGrounds.jsp">ViewBookedGrounds</a> 

		<div class="topnav-right">
			<a href="HomePage.jsp" onClick=(if(window.confirm("You are successfully Logged out")))>Logout</a>

		</div>
	</div>



	<div class="back">
		<br>
		<h1 class="head">Welcome To Admin Page</h1>
		<br> <img class="myImage" src="Images\logo.jpg" width="550px"
			height="150px" alt="pics" style="width: 20%"><br>
		<br>
		<br>
	</div>
	<section> 
	<img class="mySlides" src="Images\img9.jpg"height="300px" width="1332px">
	<img class="mySlides" src="Images\adithya3.jpg"height="300px" width="1332px">
	<img class="mySlides" src="Images\IMG8.jpg"height="300px" width="1332px">
	<img class="mySlides" src="Images\img10.jpg"height="300px" width="1332px">
		</section>
	
	<div class="footer">
		<link rel="stylesheet"
			href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<div class="cent">
			<a href="#" class="fa fa-facebook"></a> <a href="#"
				class="fa fa-twitter"></a> <a href="#" class="fa fa-google"></a> <a
				href="#" class="fa fa-yahoo"></a> <a href="#" class="fa fa-youtube"></a>
			<a href="#" class="fa fa-instagram"></a><br>
			<br>
			<br> <b class="cpy">&copy; Copyright 2018 Navodaya Sports Club.All rights Reserved</b>
		</div>
	</div>

</body>
</html>

<%-- java script --%>

<script>     
/* Automatic Slideshow - change image every 4 seconds */
var myIndex = 0;
carousel();

function carousel() {
    var i;
    var x = document.getElementsByClassName("mySlides");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";
    }
    myIndex++;
    if (myIndex > x.length) {myIndex = 1}
    x[myIndex-1].style.display = "block";
    setTimeout(carousel, 4000);
}
</script>

<%--CSS--%>

<style>
* {
	box-sizing: border-box;
}

.body {
	font-family: Arial;
}

.head {
	color: white;
	text-align: center;
}

.navbar {
	overflow: hidden;
	background-color: #3cc4a5;
	font-family: Arial, Helvetica, sans-serif;
}

.navbar a {
	float: left;
	font-size: 18px;
	color: white;
	text-align: center;
	padding: 16px 20px;
	text-decoration: none;
}

.navbar a:hover {
	background-color: purple;
}

.topnav-right {
	float: right;
}

.myImage {
	margin-left: 40%;
}

.back {
	background: #ff6666;
}

.scrollmarq {
	background: yellow;
	font-size: 40px;
}


.footer {
	width: 1945px;
	height: 250px;
	background:blue;
	-webkit-animation: mymove 5s infinite; /* Chrome, Safari, Opera */
	animation: mymove 5s infinite;
}


.fa {
	padding: 12px 16px;
	font-size: 30px;
	width: 45px;
	text-align: center;
	text-decoration: none;
	border-radius: 50%;
	
}

.fa:hover {
	opacity: 0.7;
}

.fa-facebook {
	background: #3B5998;
	color: white;
}

.fa-twitter {
	background: #55ACEE;
	color: white;
}

.fa-google {
	background: #dd4b39;
	color: white;
}

.fa-yahoo {
	background: #430297;
	color: white;
}

.fa-youtube {
	background: #bb0000;
	color: white;
}

.fa-instagram {
	background: #125688;
	color: white;
}

.cpy {
	margin-left:none;
	text-align:left;
	color:white;
	font-size:20px;
}

.cent {
	margin-left: 40%;
	padding: 14px 16px;
	
}

.scroll{
color:White;
font-size:40px;
}
}


</style>


<script>
if(${adminpage == 'true'}){
	  alert("Welcome, You are Successfully logged in...");
	}
	
</script>
