<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Sports Club management System</title>
<link rel="stylesheet" type="text/css" href="Stylesheet_AdminIndex.css">
</head>
<body>

	<div class="back">
	<button type="button" class="sbtbtm"
			onClick="window.location.href='HomePage.jsp'"><b>Back</b></button>
			
			<button type="button" class="sbtbtm"
			onClick="window.location.href='HomePage.jsp'"><b>Home</b></button><br><br>
			
			
		<br> <img class="myImage" src="Images\logo.jpg" height="250px" width="450px"><br>
			
    
	</div>	
		<%=(request.getAttribute("errMessage") == null) ? "": request.getAttribute("errMessage")%>
		
	<form name="form" action="AdminLogin" method="post"
		onsubmit="return validate()">
		
		<div class="container">
			<h1>Admin Login</h1>
			<hr>

			<label for="username"><b>Username</b></label> <input type="text"
				placeholder="Enter username" name="username"> <label
				for="password"><b>Password</b></label> <input type="password"
				placeholder="Enter password" name="password">

			
			<button type="submit" class="loginbtn">Login</button>
			<button type="button" class="cancelbtn"
				onClick="window.location.href='HomePage.jsp'">Cancel</button>

		</div>

	</form>
	
	<div class="footer">
		<link rel="stylesheet"
			href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<div class="cent">
			<a href="#" class="fa fa-facebook"></a> <a href="#"
				class="fa fa-twitter"></a> <a href="#" class="fa fa-google"></a> <a
				href="#" class="fa fa-yahoo"></a> <a href="#" class="fa fa-youtube"></a>
			<a href="#" class="fa fa-instagram"></a><br>
			<br>
			<br> <b class="cpy">&copy; Copyright 2019 Sports Club Management System.All Rights Reserved</b>
		</div>
	</div>

</body>
</html>



<script>
function validate()
{ 
var username = document.form.username.value; 
var password = document.form.password.value;
if (username==null || username=="")
{ 
alert("username cannot be blank"); 
return false; 
}
else if(password==null || password=="")
{ 
alert("password cannot be blank"); 
return false; 
} 
}
if(${admnpg == 'false'}){
	  alert("Sorry, Please enter valid details...");
	}


</script>

<style>
.container{
padding: 16px;
	text-align: center;
	display: block;
	background-color:grey;
	font-size: 18px;
	width: 100%;
	border-radius: 12px;
	box-shadow: 0px 10px 20px 15px #999999;
}
.sbtbtm{
background-color:white;
color:Black;
font-size:30px;

.back {
	background:white;
}
</style>
