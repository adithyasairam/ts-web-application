<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Sports Club Management System</title>
<link rel="stylesheet" type="text/css" href="Stylesheet_AdminIndex.css">
</head>
<body>

	<div class="back">
	<button type="button" class="sbtbtm"
			onClick="window.location.href='PlayersLogin.jsp'"><b>Back</b></button>
			
			<button type="button" class="sbtbtm"
			onClick="window.location.href='HomePage.jsp'"><b>Home</b></button><br><br>
			
			
		<br> <img class="myImage" src="Images\logo.jpg" height="250px" width="450px"><br>
			
     
	</div>	
		
	<form name="form" action="PlayersRegistration" method="post"
		onsubmit="return validate()">
		
		<div class="container">
			<h1>Players Registration</h1>
			<hr>

				
				<label for="username"><b>username</b></label> <input type="text"
				placeholder="Enter username" name="username"><br><br> 
				
				
				
				<label for="phonenumber"><b>Phone Number</b></label> <input type="text"
				placeholder="Enter phonenumber" name="phonenumber"> <br><br>
				
				<label for="gender"><b>Gender</b></label> <input type="radio"
			 name="gender" value="male">Male 
				<label for="gender"><b></b></label> <input type="radio"
				 name="gender" value="female">Female <br><br>
				 
				 
				 
				 
				 
				 <label for="age"><b>Age</b></label> <input type="text"
				placeholder="Enter age" name="age"> <br><br>
				
				 
			
				
				<label for="password"><b>Password</b></label> <input type="password"
				placeholder="Enter password" name="password"><br><br>
				
	<label for="address"><b>Address</b></label> <textarea rows="4" cols="50" placeholder="Enter address" name="address"></textarea>  <br><br>
				

			<%=(request.getAttribute("errMessage") == null) ? ""
: request.getAttribute("errMessage")%>

			<button type="submit" class="loginbtn">Register</button>
			<button type="button" class="cancelbtn"
				onClick="window.location.href='HomePage.jsp'">Cancel</button>

		</div>

	</form>
	
	<div class="footer">
		<link rel="stylesheet"
			href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<div class="cent">
			<a href="#" class="fa fa-facebook"></a> <a href="#"
				class="fa fa-twitter"></a> <a href="#" class="fa fa-google"></a> <a
				href="#" class="fa fa-yahoo"></a> <a href="#" class="fa fa-youtube"></a>
			<a href="#" class="fa fa-instagram"></a><br>
			<br>
			<br> <b class="cpy">&copy; Copyright 2019 Sports Club Management System.All Rights Reserved</b>
		</div>
	</div>

</body>
</html>

<script>
function validate()
{ 

var username = document.form.username.value;
var address = document.form.address.value;
var phonenumber = document.form.phonenumber.value;
var gender = document.form.gender.value;
var age = document.form.age.value;
var password = document.form.password.value;

if (username==null || username=="")
{ 
alert("username cannot be blank"); 
return false; 
}
else if(phonenumber==null || phonenumber=="")
{ 
alert("phonenumber cannot be blank"); 
return false; 
} 
else if(address==null || address=="")
{ 
alert("address cannot be blank"); 
return false; 
} 
else if(gender==null || gender=="")
{ 
alert("gender cannot be blank"); 
return false; 
} 

else if(age==null || age=="")
{ 
alert("age cannot be blank"); 
return false; 
} 

else if(password==null || password=="")
{ 
alert("password cannot be blank"); 
return false; 
} 
}
</script>



