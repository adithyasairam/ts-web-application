<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="java.sql.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Sports Club Management System</title>
<link rel="stylesheet" type="text/css" href="Stylesheet_AdminIndex.css">
</head>
<body>
<%! String driverName = "oracle.jdbc.driver.OracleDriver";%>
<%!String url = "jdbc:oracle:thin:@localhost:1521:xe";%>
<%!String user = "sportsclub";%>
<%!String psw = "123";%>
<div class="header">
</div>
<div class="navbar">       
		<a href="PlayersPage.jsp">Back</a> 
		</div>	
<form action="CancelBookings" method=post onsubmit = "return validate()" 
style="border:1px solid #ccc"><br><br><br><br>
<h1 align="center">CancelBookings</h1>

  <div align="center" class="container">
    
    <label for="groundname"><b>groundname</b></label>
    <%
	Connection con = null;
	PreparedStatement ps = null;
	try
	{
	Class.forName(driverName);
	con = DriverManager.getConnection(url,user,psw);
	String sql = "SELECT groundname FROM Bookings";
	ps = con.prepareStatement(sql);
	ResultSet rs = ps.executeQuery(); 
	%>
    <select name=ground>
    <option value="0">select grounds</option>
    <%
	while(rs.next())
	{
	String groundname = rs.getString(1);
	
	%>
    <option value="<%=groundname%>"><%=groundname%></option>
    <%
	}
	%>
	</select><br></br>
	<%
	}
	catch(SQLException sqe)
	{ 
	out.println(sqe);
	}
	%>
    <button type="submit">Delete</button>
  </div>

  <div align="center" class="container">
    <a href="PlayersPage.jsp">Cancel</a>
  </div>
</form>

<div class="footer">
		<link rel="stylesheet"
			href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<div class="cent">
			<a href="#" class="fa fa-facebook"></a> <a href="#"
				class="fa fa-twitter"></a> <a href="#" class="fa fa-google"></a> <a
				href="#" class="fa fa-yahoo"></a> <a href="#" class="fa fa-youtube"></a>
			<a href="#" class="fa fa-instagram"></a><br>
			<br>
			<br> <b class="cpy">&copy; Copyright 2019 Navodaya Sports Club.All Rights Reserved</b>
		</div>
	</div>

</body>
</html>

<style>
.header {
  overflow: hidden;
  background-color: #f1f1f1; /*background colour for header is grey*/
  padding: 10px 10px;
}
.navbar {
	overflow: hidden;
	background-color:#33CDF7;
	font-family: Arial, Helvetica, sans-serif;
}
.navbar a {
	float: left;
	font-size: 18px;
	color: black;
	text-align: center;
	padding: 16px 20px;
	text-decoration: none;
}

.navbar a:hover {
	background-color:#F38A14;
}

body {font-family: Arial, Helvetica, sans-serif;}
form {background: url('book.jpg');
background-size: 1400px 800px;
border: 3px solid #f1f1f1;}

input[type=text]{
  width: 30%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  box-sizing: border-box;
}


select,option{
	width:30%;
	padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  box-sizing: border-box;
}


button {
  background-color: #4CAF50;
  color: white;
  padding: 15px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 10%;
}

button:hover {
  opacity: 0.8;
}

.cancelbtn {
  width: auto;
  padding: 10px 18px;
  background-color: white;
}

.container {
  padding: 30px;
}

/* Change styles cancel button and Register button on extra small screens */
@media screen and (max-width: 300px) {
  
  .a {
  border: none;
  float: left;
  font-size: 16px;
  color: black;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}
}



.footer {
	width: 1332px;
	height: 220px;
	background:#f1f1f1;
	
}

.cpy {
	margin-left:none;
	text-align:left;
	color:black;
	font-size:20px;
}

.cent {
	margin-left: 30%;
	padding: 14px 16px;
	
}


</style>