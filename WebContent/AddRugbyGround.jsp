<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Sports Club Management System</title>
<link rel="stylesheet" type="text/css" href="Stylesheet_AdminIndex.css">
</head>
<body>

<form name="form" action="AddRugbyGround" method="post"
		onsubmit="return validate()">
		
		<div class="container">
			<h1>AddRugbyGround</h1>
			<hr>

				
				<label for="groundname"><b>GroundName</b></label> <input type="text"
				placeholder="Enter groundname" name="groundname"><br><br> 
				
				
				
				<label for="area"><b>Area</b></label> <input type="text"
				placeholder="Enter area" name="area"> <br><br>
				
				 
				 
				 
				 
				 <label for="city"><b>City</b></label> <input type="text"
				placeholder="Enter city" name="city"> <br><br>
				 
			
				
				<label for="cost"><b>Cost</b></label> <input type="text"
				placeholder="Enter cost" name="cost"><br><br>
				
				
				<label for="address"><b>Address</b></label> <textarea rows="4" cols="50" placeholder="Enter address" name="address"></textarea>  <br><br>
				

			<%=(request.getAttribute("errMessage") == null) ? ""
: request.getAttribute("errMessage")%>

			<button type="submit" class="loginbtn">Add</button>
			<button type="button" class="cancelbtn"
				onClick="window.location.href='Rugby.jsp'">Cancel</button>

		</div>
		</form>
	
	<div class="footer">
		<link rel="stylesheet"
			href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<div class="cent">
			<a href="#" class="fa fa-facebook"></a> <a href="#"
				class="fa fa-twitter"></a> <a href="#" class="fa fa-google"></a> <a
				href="#" class="fa fa-yahoo"></a> <a href="#" class="fa fa-youtube"></a>
			<a href="#" class="fa fa-instagram"></a><br>
			<br>
			<br> <b class="cpy">&copy; Copyright 2019 Sports Club Management System.All Rights Reserved</b>
		</div>
	</div>

</body>
</html>

<script>
function validate()
{ 

var groundname = document.form.groundname.value;
var address = document.form.address.value;
var area = document.form.area.value;
var city = document.form.city.value;
var cost = document.form.cost.value;

if (groundname==null || groundname=="")
{ 
alert("groundname cannot be blank"); 
return false; 
}
else if(address==null || address=="")
{ 
alert("address cannot be blank"); 
return false; 
} 
else if(area==null || area=="")
{ 
alert("area cannot be blank"); 
return false; 
}

else if(city==null || city=="")
{ 
alert("city cannot be blank"); 
return false; 
} 

else if(cost==null || cost=="")
{ 
alert("cost cannot be blank"); 
return false; 
} 
}
</script>



